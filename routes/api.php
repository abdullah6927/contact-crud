<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('contact', [App\Http\Controllers\ApiController::class, 'createContact' ])->name('createContact');
Route::get('contact', [App\Http\Controllers\ApiController::class, 'getAllContacts' ])->name('getAllContacts');
Route::get('contact/{id}', [App\Http\Controllers\ApiController::class, 'getContact' ])->name('getContact');
Route::put('contact/{id}', [App\Http\Controllers\ApiController::class, 'updateContact' ])->name('updateContact');
Route::Delete('contact/{id}', [App\Http\Controllers\ApiController::class, 'deleteContact' ])->name('deleteContact');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
