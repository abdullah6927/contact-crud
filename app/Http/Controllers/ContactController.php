<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Auth;
use Illuminate\Support\Facades\Session;


class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $contact = Contact::where('user_id', Auth::id())->get();
        return view('dashboard', ['contact' => $contact] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'fname'=> 'required',
            'lname'=> 'required',
            'contact'=> 'required',
        ]);
        $contact = new Contact();

        $contact->user_id = Auth::id();
        $contact->fname = request('fname');
        $contact->lname = request('lname');
        $contact->contact_number = request('contact_number');


        // $message = Session::flash('message','This is a message!'); 
        // dd($message);
        $contact->save();
        Session::flash('message', 'Record is added'); 
        // $contact = Contact::where('user_id', Auth::id())->get();
        // return view('/dashboard', compact($contact))->with('success', 'it abdullah ');
        // return view('/dashboard', ['contact' => $contact])->response()->jason(['message'=> 'Contact added Successfully'], 200);
        return redirect(route('contact.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::find($id);

        return view('crud.edit', ['contact' => $contact] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::find($id);

        $contact->user_id = Auth::id();
        $contact->fname = request('fname');
        $contact->lname = request('lname');
        $contact->contact_number = request('contact');
        
        $contact->save();
        return redirect(route('contact.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        return redirect(route('contact.index'));
    }
}
