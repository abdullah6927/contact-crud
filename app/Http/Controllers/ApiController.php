<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use Auth;

class ApiController extends Controller
{
    public function getAllContacts() {
    	$contacts = Contact::get()->toJson(JSON_PRETTY_PRINT);
    	return response($contacts, 200);
    }

    public function createContact(Request $request) {
    	$contact = new Contact();

        $contact->user_id = 1;
        $contact->fname = request('fname');
        $contact->lname = request('lname');
        $contact->contact_number = request('contact_number');

        $contact->save();
        return response()->json(['message'=> 'Contact added Successfully'], 200);
    }

    public function getContact($id) 
    {
    	if (Contact::where('id', $id)->exists()) {
    		$contact = Contact::where('id', $id)->get()->toJson(JSON_PRETTY_PRINT);
    		return response($contact, 200);
    	}
    	else {
    		return response()->json([
    			"message" => "Record not found"
    		], 404);
    	}
    }

    public function updateContact(Request $request, $id) 
    {
    	if (Contact::where('id', $id)->exists()) {
    		$contact = Contact::find($id);

    		$contact->user_id 			=  1;
    		$contact->fname 			=  $request->fname;
    		$contact->lname 			=  $request->lname;
    		$contact->contact_number 	=  $request->contact_number;

    		$contact->save();
    		return response()->json([
    			"message" => "records updated successfully"
    		], 200);
    	}
    	else 
    	{
        	return response()->json([
            	"message" => "contact not found"
        	], 404);
        }
    }

    public function deleteContact ($id) 
    {
    	if(Contact::where('id', $id)->exists()) {
        	$contact = Contact::find($id);
        	$contact->delete();

        	return response()->json([
          		"message" => "records deleted"
        	], 202);
      	} 
      	else {
        	return response()->json([
          		"message" => "contact not found"
        	], 404);
      	}
    }
}
